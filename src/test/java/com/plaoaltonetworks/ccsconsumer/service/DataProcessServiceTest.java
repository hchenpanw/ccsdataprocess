package com.plaoaltonetworks.ccsconsumer.service;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.plaoaltonetworks.ccsconsumer.config.SparkConfiguration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.hadoop.mapred.JobConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.jupiter.api.Test;
import scala.Tuple2;


public class DataProcessServiceTest {

  @Test
  public void test() throws ClassNotFoundException{
    System.out.println("===>test");
    JavaSparkContext sparkContext = SparkConfiguration.buildLocalSparkContext("CCS processor");
    JobConf jobConf = SparkConfiguration.buildLocalJobConf(sparkContext, Boolean.TRUE);
    System.out.println("conf:" + jobConf.toString());

    Map<String, AttributeValue> map1 = new HashMap<String, AttributeValue> ();
    map1.put("payload", new AttributeValue("{\"customerId\":9,\"action\":\"UPSERT\",\"recordChangeTs\":1626251881596,"
        + "\"type\":\"RESOURCE\",\"recordId\":\"187289\",\"prismaId\":\"804257783817005056\",\"previous\":null,\"current\":{\"customerId\":9,\"prismaTenantId\":804257783817005056,\"accountId\":\"496947949261\",\"customerName\":\"tw-qa-5\",\"cloudType\":\"aws\",\"redlockApiId\":5046,\"redlockApiName\":\"aws-cloudwatch-log-group\",\"resourceId\":187289,\"messageType\":\"UPSERT\",\"payload\":{\"region\":\"us-east-1\",\"name\":\"/aws/lambda/test_python_file_system_embed_alert_001802_2f6\",\"uniqueResourceName\":\"arn:aws:logs:us-east-1:496947949261:log-group:/aws/lambda/test_python_file_system_embed_alert_001802_2f6:*\",\"resourceCspUrl\":\"\",\"vpc\":\"\",\"ingestionTime\":1625829820000,\"updatedTime\":1625829820000,\"kvs\":{},\"json\":\"{\\\"arn\\\":\\\"arn:aws:logs:us-east-1:496947949261:log-group:/aws/lambda/test_python_file_system_embed_alert_001802_2f6:*\\\",\\\"creationTime\\\":1616717893446,\\\"logGroupName\\\":\\\"/aws/lambda/test_python_file_system_embed_alert_001802_2f6\\\",\\\"metricFilterCount\\\":0,\\\"tags\\\":[]}\",\"eventIds\":[]},\"jobId\":\"00000000-0000-4000-0000-000000000000\"},\"callbackURL\":null,\"truncated\":false}"));


    Map<String, AttributeValue> map2 = new HashMap<String, AttributeValue> ();
    map2.put("payload", new AttributeValue("{\"customerId\":9,\"action\":\"DELETED\","
        + "\"recordChangeTs\":1626266507486,\"type\":\"RESOURCE\",\"recordId\":\"19115\",\"prismaId\":\"804257783817005056\",\"previous\":{\"customerId\":9,\"prismaTenantId\":804257783817005056,\"accountId\":\"496947949261\",\"customerName\":\"tw-qa-5\",\"cloudType\":\"aws\",\"redlockApiId\":13,\"redlockApiName\":\"aws-iam-list-attached-user-policies\",\"resourceId\":19115,\"messageType\":\"DELETE\",\"payload\":{\"deletedOn\":1626266278397,\"eventIds\":[]},\"jobId\":\"00000000-0000-4000-0000-000000000000\"},\"current\":null,\"callbackURL\":null,\"truncated\":false}"));



    JavaRDD<Tuple2<String, Map<String, AttributeValue>>> records = sparkContext.parallelize(
       Arrays.asList( new Tuple2("1#r1", map1), new Tuple2("1#r1", map2))
   );
    System.out.println("=======original records");
    records.collect().forEach( r -> System.out.println(r));

    System.out.println("=======after  process");
    new DataProcessService().extractEventLifeCycle(records).collect().forEach( r -> System.out.println(r));

    sparkContext.stop();
  }

}



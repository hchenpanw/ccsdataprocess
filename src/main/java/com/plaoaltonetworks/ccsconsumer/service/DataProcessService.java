package com.plaoaltonetworks.ccsconsumer.service;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.plaoaltonetworks.ccsconsumer.config.SparkConfiguration;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.hadoop.dynamodb.DynamoDBItemWritable;
import org.apache.hadoop.dynamodb.read.DynamoDBInputFormat;
import org.apache.hadoop.mapred.JobConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.hadoop.io.Text;
import scala.Tuple1;
import scala.Tuple2;
import scala.Tuple3;
@Log4j2
public class DataProcessService implements Serializable {
  private static final Configuration conf =
      Configuration.defaultConfiguration()
          .addOptions(Option.DEFAULT_PATH_LEAF_TO_NULL)
          .addOptions(Option.SUPPRESS_EXCEPTIONS);

  public void process() throws ClassNotFoundException {
    SparkSession sparkContext = SparkConfiguration.buildSparkSession("CCS processor");
    JobConf jobConf = SparkConfiguration.buildJobConf(sparkContext.sparkContext().hadoopConfiguration(), Boolean.TRUE);
    System.out.println("conf:" + jobConf.toString());

    // Building a RDD pointing to the DynamoDB table
    JavaRDD<Tuple2<Text, DynamoDBItemWritable>> totalRecords = sparkContext.sparkContext().hadoopRDD(jobConf,
        DynamoDBInputFormat.class,
        Text.class, DynamoDBItemWritable.class,4).toJavaRDD();

    System.out.println("Citations count: " + totalRecords.count());
    //citations.map(r -> r._2.getItem().get("payload")).collect().forEach(v-> System.out.println(extractResourceName
    // (v.getS())));
    //<PrismaId#recordId : ActionType(UpInsert/DELETE # Timestamp)

    JavaRDD<Tuple2<String, Map<String, AttributeValue>>> records = totalRecords.map(r -> new Tuple2(r._2.getItem().get(
        "recordId").getS(), r._2.getItem()));

    JavaRDD<EventLifeCycle> evtLifecycles = extractEventLifeCycle(records);

    evtLifecycles.collect().forEach(r->System.out.println(r));

    Dataset<Row> evtDF = sparkContext.createDataFrame(evtLifecycles, EventLifeCycle.class);
    Properties connectionProperties = new Properties();
    connectionProperties.put("user", "mainuser");
    connectionProperties.put("password", "mainuser");
    connectionProperties.put("driver", "org.postgresql.Driver");
    evtDF.write().mode("overwrite").jdbc("jdbc:postgresql://localhost:2002/master", "eventcycle", connectionProperties);

   // Dataset<Row> peopleDF = spark.createDataFrame(   extractEventLifeCycle(records), EventLifeCycle.class);

  /*  JavaPairRDD<String, Tuple2> res   = totalRecords.mapToPair(r ->  new Tuple2(r._2.getItem().get(
        "recordId").getS(), new Tuple2(extractActionType(r._2.getItem().get("payload").getS()),
        extractDeleteTimeStamp(r._2.getItem().get("payload").getS()))));*/

   // JavaPairRDD<String,EventLifeCycle > finalres =
   /*     res.aggregateByKey(new EventLifeCycle(), (a, b) -> extractStartEndTime(a
        , b), (a, b) -> mergeEventLifeCycle(a, b) ).filter(r->r._2.startTime != null && r._2.endTime!= null).collect()
        .forEach(r->System.out.println(r));*/

   // .a(r-> new Tuple2(r._1,
    //    extractStartEndTime(r._2)));


   /* JavaPairRDD<String, String> res =
        totalRecords.filter(r-> isAllowedAPI(r._2.getItem().get("payload").getS())).mapToPair(r -> new Tuple2(extractPrismaId(r._2.getItem().get(
        "recordId").getS()),
            extractdbeName(r._2.getItem().get("payload").getS())));

    res.aggregateByKey(new HashSet<String>(),
        (set,v)->{if(isAllowed(v)) set.add(v); return set;},
        (set, v) ->{ set.addAll(v.stream().filter(name -> isAllowed(name)).collect(Collectors.toSet())); return set;})
        .collect().forEach(r-> System.out.println(r));*/

  }

  public JavaRDD<EventLifeCycle> extractEventLifeCycle(
      JavaRDD<Tuple2<String, Map<String, AttributeValue>>> records) {
    JavaPairRDD<String, Tuple2> res   = records.mapToPair(  r-> new Tuple2(r._1,
          new Tuple2(extractActionType(r._2.get("payload").getS()),
              extractUpdateTimeStamp(r._2.get("payload").getS()))));

    JavaRDD<EventLifeCycle> filter;
    filter = res.aggregateByKey(new EventLifeCycle(),
        (a, b) -> extractStartEndTime(a
        , b), (a, b) -> mergeEventLifeCycle(a, b) )
        .filter(r->r._2.getStartTime() != null && r._2.getEndTime()!= null)
        .map(r->{r._2.setRecordId( extractRecordId(r._1)); r._2.setPrismaId(extractPrismaId(r._1)); return r._2;});

    return filter;

  }

  private EventLifeCycle extractStartEndTime(EventLifeCycle lifecycle, Tuple2 evt){

      if("DELETE".equals(evt._1)){
        lifecycle.setEndTime( ((Long)evt._2));
      }else{
        if(lifecycle.getStartTime()  == null || (evt._2 != null && lifecycle.getStartTime() > (Long)evt._2)){
          lifecycle.setStartTime((Long)evt._2);
        }
      }

    return lifecycle;
  }

  private EventLifeCycle mergeEventLifeCycle(EventLifeCycle lifecycle1, EventLifeCycle lifecycle2){
    EventLifeCycle newLifeCycle = new EventLifeCycle();
    if(lifecycle1.getStartTime() != null && lifecycle2.getStartTime() != null) {
      newLifeCycle.setStartTime(Math.min(lifecycle1.getStartTime() , lifecycle2.getStartTime() ));
    }else  if(lifecycle1.getStartTime() == null ){
      newLifeCycle.setStartTime(lifecycle2.getStartTime());
    }else{
      newLifeCycle.setStartTime(lifecycle1.getStartTime());
    }
    if(lifecycle1.getEndTime() != null && lifecycle2.getEndTime() != null) {
      newLifeCycle.setEndTime(Math.min(lifecycle1.getEndTime() , lifecycle2.getEndTime())) ;
    }else  if(lifecycle1.getEndTime() == null){
      newLifeCycle.setEndTime(lifecycle2.getEndTime());
    }else{
      newLifeCycle.setEndTime(lifecycle1.getEndTime());
    }
    return newLifeCycle;

  }

  private Tuple2 extractStartEndTime(Iterable<Tuple2> evts){
    Long startTime = null;
    Long endTime  = null;
    for(Tuple2 evt  :evts){
     if("DELETED".equals(evt._1)){
       endTime = (Long)evt._2;
     }else{
       if(startTime  == null || (startTime > (Long)evt._2)){
         startTime = (Long)evt._2;
       }
     }
   }
    return new Tuple2(startTime, endTime);
  }

 private boolean isAllowedAPI(String jsonPayload){
   String apiname = "aws-rds-describe-db-instances";
   String key1 = "current.redlockApiName";
   String key2 = "previous.redlockApiName";

   String value = JsonPath.using(conf).parse(jsonPayload).read(key1);
   if(value == null ) {
     value = JsonPath.using(conf).parse(jsonPayload).read(key2);;
   }
   if(apiname.equals(value)){
     return true;
   }

    return false;

 }



  private boolean isAllowed(String name){
    if(name == null){
      return false;
    }
    List<String> blacklist = Arrays.asList( new String[] {"policy"});
    if(blacklist.stream().anyMatch(f -> name.contains(f))){
      return false;
    }
    return true;

  }
  private String extractPrismaId(String key ) {
    if(key == null || key.length() == 0){
        return "";
    }
    return key.split("#")[0];

  }
  private String extractRecordId(String key ) {
    if(key == null || key.length() == 0){
      return "";
    }
    return key.split("#")[1];

  }
  private String extractResourceName(String jsonPayload ) {
    String key1 = "current.payload.uniqueResourceName";
    String key2 = "previous.payload.uniqueResourceName";

    String value1 = JsonPath.using(conf).parse(jsonPayload).read(key1);
    if(value1 != null ) {
      return value1;
    }
    return JsonPath.using(conf).parse(jsonPayload).read(key2);

  }

  private String extractActionType(String jsonPayload ) {
    String actionType =  (String) extractValue(jsonPayload, "messageType");
    return actionType;

  }
  private Long extractUpdateTimeStamp(String jsonPayload ) {
    String key1 = "current.messageType";
    String key2 = "previous.messageType";
    String actionType = JsonPath.using(conf).parse(jsonPayload).read(key1);
    if(actionType == null ) {
      actionType = JsonPath.using(conf).parse(jsonPayload).read(key2);
    }
    Object res = null;
    if("DELETE".equals(actionType)){
      res = extractValue(jsonPayload, "payload.deletedOn");
    }else{
      res = extractValue(jsonPayload, "payload.updatedTime");
    }
    if(res == null){
      return null;
    }
    return Long.parseLong("" + res);

  }
  private Object extractValue(String jsonPayload, String key ) {
    String key1 = "current." + key;
    String key2 = "previous." + key;

    Object value1 = JsonPath.using(conf).parse(jsonPayload).read(key1);
    if(value1 != null ) {
      return value1;
    }
    Object res =  JsonPath.using(conf).parse(jsonPayload).read(key2);
     System.out.println("==>key:" + key + ", obj:" + res);
     return res;
  }

  private Object extractDeleteTimeStamp(String jsonPayload ) {
    String key1 = "current.payload.json.deletedOn";
    String key2 = "previous.payload.json.deletedOn";

    Object value1 = JsonPath.using(conf).parse(jsonPayload).read(key1);
    if(value1 != null ) {
      return value1;
    }
    return JsonPath.using(conf).parse(jsonPayload).read(key2);

  }


  private String extractdbeName(String jsonPayload ) {
    String key1 = "current.payload.json.dbparameterGroups.dbparameterGroups";
    String key2 = "previous.payload.json.dbparameterGroups.dbparameterGroups";

    String value1 = JsonPath.using(conf).parse(jsonPayload).read(key1);
    if(value1 != null ) {
      return value1;
    }
    return JsonPath.using(conf).parse(jsonPayload).read(key2);

  }

  public static void main(String[] args) throws ClassNotFoundException {
    new DataProcessService().process();
  }

}

package com.plaoaltonetworks.ccsconsumer.service;

import java.io.Serializable;
import lombok.Data;
import lombok.Getter;

public class EventLifeCycle implements Serializable{

  private Long startTime;
  private Long endTime;
  private String prismaId;
  private String recordId;

  public EventLifeCycle() {
  }

  public Long getStartTime() {
    return startTime;
  }

  public void setStartTime(Long startTime) {
    this.startTime = startTime;
  }

  public Long getEndTime() {
    return endTime;
  }

  public void setEndTime(Long endTime) {
    this.endTime = endTime;
  }

  public String getPrismaId() {
    return prismaId;
  }

  public void setPrismaId(String prismaId) {
    this.prismaId = prismaId;
  }

  public String getRecordId() {
    return recordId;
  }

  public void setRecordId(String recordId) {
    this.recordId = recordId;
  }



}